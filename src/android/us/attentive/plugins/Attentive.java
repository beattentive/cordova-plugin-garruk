package us.attentive.plugins;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import us.attentive.garruk.BuildConfig;

public class Attentive extends CordovaPlugin {

  private static final String TAG = "Attentive";

  private static final String PREFERENCES = "us.attentive.garruk.preferences";
  private static final String TOKEN = "token";

  private static final String ACTION_SHARE_ARTICLE = "share";
  private static final String ACTION_SET_TOKEN = "settoken";
  private static final String ACTION_LOGOUT = "logout";
  private static final String ACTION_GET_VERSION = "app_version";

  @Override
  protected void pluginInitialize() {
    Log.d(TAG, "initializing plugin");
  }

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        Log.d("Attentive", "Executing plugin action " + action);

        final JSONObject options = args.getJSONObject(0);

        if(ACTION_SHARE_ARTICLE.equals(action)){
          // SHARE THE ARTICLE WITH
          final String url = options.getString("url");
          final String title = options.getString("title");
          shareArticle(url,title);
          return true;
        }

        if(ACTION_SET_TOKEN.equals(action)){
          final String token = options.getString("token");
          setToken(token);
          return true;
        }

        if(ACTION_LOGOUT.equals(action)){
          logout();
          return true;
        }

        if(ACTION_GET_VERSION.equals(action)){
            callbackContext.success(app_version());
        }

        return false;
    }

  @Override
  public void onPause(boolean multitasking) {
  }

  @Override
  public void onResume(boolean multitasking) {
  }

  public void shareArticle(String url, String title) {
    // SHARE THE THING!!!
    Intent i = new Intent(Intent.ACTION_SEND);
    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
    i.setType("text/plain");
    if (title != null) {
      i.putExtra(Intent.EXTRA_SUBJECT, title);
    }
    i.putExtra(Intent.EXTRA_TEXT, url.replace("attentive.us/a/", "attentive.us/s/"));
    cordova.getActivity().startActivity(Intent.createChooser(i, "Choose!"));
  }

  public void setToken(String value){
    Log.d(TAG, "saving token " + value);
    Activity activity = cordova.getActivity();
    SharedPreferences sharedPref = activity.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPref.edit();
    editor.putString(TOKEN, value);
    editor.apply();
  }

  public void logout(){
    Log.d(TAG, "loggging out");
    Activity activity = cordova.getActivity();
    SharedPreferences sharedPref = activity.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPref.edit();
    editor.clear();
    editor.apply();
  }

  public boolean is_debug(){
    return BuildConfig.DEBUG;
  }

  public int app_version(){
    return BuildConfig.VERSION_CODE;
  }

}